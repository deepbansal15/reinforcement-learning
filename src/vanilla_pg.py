import tensorflow as tf
import numpy as np
import gym
import time

"""
Gym Env
step,reset,render,close,seed are the functions from an environment
"""
def gym_test(env_name="CartPole-v0"):
    env = gym.make(env_name)
    # Episodes ran
    for episode_i in range(100):
        observation = env.reset()
        # Horizon or trajectory of 100
        for i in range(100):
            env.render()
            print(observation)
            action = env.action_space.sample()
            observation, reward,done,info = env.step(action)
            if done:
                print("Episode {} finished after {} steps".format(episode_i+1,i+1))
                break

"""
sizes = [hidden_size,action_space_size]
"""
def mlp(x,sizes,activation=tf.tanh,output_activation=None):

    # create layers from input to last hidden layer
    for size in sizes[:-1]:
        # tf.dense(input,output_size,activation)
        x = tf.layers.dense(x,units=size,activation=activation)
    
    # create output layer
    # normally you don't give output_activation because it makes pi(x;theta) non differentiable
    return tf.layers.dense(x,units=sizes[-1],activation=output_activation)

"""
Vanila policy gradient 
hidden_layers = hidden layers size in neural network
epi_len = number of samples to take for calculation of mean higher is always better because of law of infinite numbers
epochs = number of times to update the neural network (policy network)
"""
def train(env_name="CartPole-v0",hidden_layers=32,epi_len=5000,epochs=30,learning_rate=1e-2):

    # make environment and get observation and action space sizes
    env = gym.make(env_name)
    obs_dim = env.observation_space.shape[0]
    n_acts = env.action_space.n

    mlp_size = [hidden_layers] + [n_acts]
    
    # placeholder for observations obtained during trajectory over multiple episodes
    # observations over 1 epoch
    obs_ph = tf.placeholder(shape=(None,obs_dim),dtype=tf.float32)
    # logits from neural network un-normalized and  don't sumup to 1 (not probabilities)
    logits = mlp(obs_ph,mlp_size)

    # convert logits to actions
    # actions over 1 epoch
    # tf.squeeze is converting actions in 2d to 1d [[action]] to [action]
    actions = tf.squeeze(tf.multinomial(logits=logits,num_samples=1),axis=1)

    # calculations for neural network back propagation
    # for more details (Gradient Estimation Using Stochastic Computation Graphs):
    # https://arxiv.org/abs/1506.05254

    # probabilities of all the actions
    y = tf.nn.log_softmax(logits)

    #placeholder for all the actions taken over an epoch
    actions_ph = tf.placeholder(shape=(None,),dtype=tf.int32)
    actions_mask = tf.one_hot(actions_ph, n_acts)
    # get log probability of the action that we actually took
    logprob_y = tf.reduce_sum(actions_mask * y,axis=1)

    # total reward over a trajectory
    # column count to epi_len
    reward_weights_ph = tf.placeholder(shape=(None,),dtype=tf.float32)
    loss = -tf.reduce_mean(logprob_y*reward_weights_ph)

    # training operation
    train_op = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss)

    # create tensorflow session
    sess = tf.InteractiveSession()
    # initialize all the global variables inside tensorflow
    sess.run(tf.global_variables_initializer())

    def train_one_epoch():
        ep_actions = []
    
        batch_obs = []
        batch_actions = []
        # also called weights in a lot of literature but it's unnecessary
        batch_reward_weights = []

        # trajectory length of each episode
        trajectory_len = []
        trajectory_reward = []
        # Loop for multiple episode or a batch in one epoch
        while len(batch_obs) < epi_len :

            # reset the environment before start of each episode or trajectory
            obs = env.reset()

            # rewards gathered along the trajectory
            ep_rewards = []

            # gather the trajectory or run a episode
            while True:
                # current observation
                batch_obs.append(obs.copy())

                # sample action from the policy
                action = sess.run(actions,{obs_ph:obs.reshape(1,-1)})[0]
                batch_actions.append(action)

                # act in the environment and gather reward and next observation
                obs,reward,done,info = env.step(action)
                ep_rewards.append(reward)
                #print(obs)
                if done:
                    #print('epsiode finished')
                    break

            # no causality assumption
            ep_total_ret, ep_len = sum(ep_rewards), len(ep_rewards)
            # save trajectory length of each episode
            trajectory_len += [ep_len]
            trajectory_reward += [ep_total_ret]
            # same weights for the whole episode
            batch_reward_weights += [ep_total_ret] * ep_len
        
        # train on gathered experience equivalent to one epoch
        batch_loss, _ = sess.run([loss,train_op],
                                 feed_dict={
                                     obs_ph:np.array(batch_obs),
                                     actions_ph:np.array(batch_actions),
                                     reward_weights_ph: np.array(batch_reward_weights)
                                 })

        return batch_loss,trajectory_len,trajectory_reward

    
    for i in range(epochs):
        #start = time.time()
        batch_loss,batch_len,batch_reward = train_one_epoch()
        #end = time.time()
        # print('time taken: ',end-start)
        print('epoch: %3d \t loss: %.3f \t average reward: %.3f \t average episode length: %.3f'%
                (i, batch_loss, np.mean(batch_reward), np.mean(batch_len)))

    # visualize

    for episode_i in range(10):
        obs = env.reset()
        # Horizon or trajectory of 100
        for i in range(500):
            env.render()
            action = sess.run(actions,{obs_ph:obs.reshape(1,-1)})[0]
            obs, reward,done,info = env.step(action)
            if done:
                print("Episode {} finished after {} steps".format(episode_i+1,i+1))
                break

    # shutdown the env
    env.close()



if __name__ == '__main__':
    # Lazy loading
    import argparse
    train()